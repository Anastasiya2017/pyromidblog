import React from "react"
import {Link, useStaticQuery, graphql} from "gatsby"
import Navigation from "../components/navigation"
import 'prismjs/themes/prism-okaidia.css';
import logoSrc from "../images/pyramid.svg";
import instagram from "../images/instagram_w.svg";
import telegram from "../images/telegram_w.svg";
import youtube from "../images/youTube_w.svg";
import {library} from '@fortawesome/fontawesome-svg-core'
import {fab} from '@fortawesome/free-brands-svg-icons'
import {fas} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

library.add(fab, fas);

export default ({children}) => {

    const data = useStaticQuery(
        graphql`
            query {
                site {
                    siteMetadata {
                        title
                        link {
                            youTube
                            inst
                            telegrPyromidInvest
                            telegrAqvaspirt
                            subscr
                        }
                    }
                }
            }
        `
    )
    const youTube = data.site.siteMetadata.link.youTube
    const inst = data.site.siteMetadata.link.inst
    const telegr = data.site.siteMetadata.link.telegrPyromidInvest
    const telegrFooter = data.site.siteMetadata.link.telegrAqvaspirt
    const subscr = data.site.siteMetadata.link.subscr

    return (
        <div>

            <header className="site-header">

                <div className="site-navigation">
                    <div>
                        <div className="site-main">
                            <Link to="/"> <img className="img-logo" src={logoSrc} alt="logo"/>
                                <div className="logoText">{data.site.siteMetadata.title}</div>
                            </Link>
                        </div>
                        <Navigation/>
                    </div>

                    <div className="site-social">
                        <a href={inst} target="_blank"><img className="site-icon" src={instagram} alt="instagram"/></a>
                        <a href={telegr} target="_blank"><img className="site-icon" src={telegram} alt="telegram"/></a>
                        <a href={youTube}target="_blank"><img className="site-icon" src={youtube} alt="youtube"/></a>
                        <a href={subscr} target="_blank" className="button -subscribe">Подписаться</a>
                    </div>
                </div>
              
            </header>
            <div className="sticky">
                <div className="site-social-box">
                    <a href={inst} target="_blank"><img className="site-icon-box" src={instagram} alt="instagram"/></a>
                    <a href={telegr} target="_blank"><img className="site-icon-box" src={telegram} alt="telegram"/></a>
                    <a href={youTube}target="_blank"><img className="site-icon-box" src={youtube} alt="youtube"/></a>
                    <a href={subscr} target="_blank" className="button -subscribe">Подписаться</a>
                </div>
            </div>
            <div className="site-wrapper">
                {children}
                <footer className="site-footer">
                    <p>Для связи&nbsp;→<FontAwesomeIcon className="site-icon-black" icon={"envelope"} size="1x"/>
                        me@pyromid.ru
                        <a href={telegrFooter} target="_blank">
                            <FontAwesomeIcon className="site-icon-black" icon={["fab", "telegram-plane"]}/>
                            Telegram</a>
                    </p>
                    <div className="footer-contact"> Пожалуйста, ставьте ссылку на pyromid.ru при использовании
                        материалов и цитировании.
                        Перепечатка постов возможна, но с разрешения. Пишите в <a href={telegr}>Telegram</a> или почту.
                    </div>
                </footer>
                <footer className="site-footer-mob">
                    <p>Для связи ↓ <br/>
                        <FontAwesomeIcon className="site-icon-black" icon={"envelope"} size="1x"/>
                        me@pyromid.ru
                        <a href="https://web.telegram.org/#/login" target="_blank">
                            <FontAwesomeIcon className="site-icon-black" icon={["fab", "telegram-plane"]}/>
                            Telegram</a>
                    </p>
                    <div className="footer-contact"> Пожалуйста, ставьте ссылку на pyromid.ru при использовании
                        материалов и цитировании.
                        Перепечатка постов возможна, но с разрешения. Пишите в <a href={telegrFooter}>Telegram</a> или почту.
                    </div>
                </footer>
            </div>
        </div>
    )
}