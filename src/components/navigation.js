import React from "react"
import {graphql, Link, StaticQuery} from "gatsby"

export default () => (
<StaticQuery
    query={graphql`
      query HeadingQuery2 {
        allMarkdownRemark(filter: {frontmatter: {menu: {eq: "yes"}}}, limit: 1) {
          edges {
              node {
                  frontmatter {
                    template
                    title
                  }
              }
          }
        }
      }
    `}
    render={data => (
        <nav className="navigation">
            <Link to="/invest/">Инвестиции</Link>
            <Link to="/plan/">Схемы</Link>
            {!!data.allMarkdownRemark.edges.length && (<Link className="tag-text" to={'/' + data.allMarkdownRemark.edges[0].node.frontmatter.title.toLowerCase() + '/'}>
                {/*{!!data.allMarkdownRemark.edges.length && (<Link className="tag-text" to="/qubittech/">*/}
                {/*QUBITTECH*/}
                {data.allMarkdownRemark.edges[0].node.frontmatter.title.toUpperCase()}
            </Link>)}
        </nav>
    )
    }
/>)