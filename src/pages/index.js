import React from "react"
import Helmet from 'react-helmet';
import {graphql, Link} from 'gatsby'
import Layout from "../components/layout"
import PostLink from "../components/post-link"

const IndexPage = ({
                       data: {
                           site,
                           allMarkdownRemark: {edges},
                           allFile
                       },
                   }) => {
    let siteUrl = site.siteMetadata.siteUrl
    let PostsInvest = edges
        .filter(edge => edge.node.frontmatter.category === 'invest')
        .sort((a, b) => a.node.frontmatter.date < b.node.frontmatter.date ? -1 : 1)
    let i = 0
    PostsInvest = PostsInvest.map(edge => {
        i++
        if (i < 7) {
            let file = allFile.edges.filter(item => item.node.absolutePath.indexOf(edge.node.frontmatter.thumbnail) !== -1)
            if (file[0] && file[0].node && file[0].node.publicURL) {
                edge.node.frontmatter.thumbnail = file[0].node.publicURL
            }
            return <PostLink key={edge.node.id} post={edge.node}/>;
        }
        return null;
    })

    i = 0
    let PostsIncome = edges
        .filter(edge => edge.node.frontmatter.category === 'plan')
        .sort((a, b) => a.node.frontmatter.date < b.node.frontmatter.date ? -1 : 1)
    PostsIncome = PostsIncome.map(edge => {
        i++
        if (i < 7) {
            let file = allFile.edges.filter(item => item.node.absolutePath.indexOf(edge.node.frontmatter.thumbnail) !== -1)
            if (file[0] && file[0].node && file[0].node.publicURL) {
                edge.node.frontmatter.thumbnail = file[0].node.publicURL
            }
            return <PostLink key={edge.node.id} post={edge.node}/>;
        }
        return null;
    })

    const telegr = site.siteMetadata.link.telegrAqvaspirt
    return (
        <Layout>
            <Helmet>
                <title>Текст</title>
                <meta name="description" content="Текст"/>
                <meta name="keywords" content="текст, текст2"/>
                <meta property="og:url" content={siteUrl}/>
                <meta property="og:type" content="website"/>
                <meta property="og:image" content="Ссылка на изображение"/>
                <meta property="og:image:width" content={site.siteMetadata.image_width}/>
                <meta property="og:image:height" content={site.siteMetadata.image_height}/>
                <meta property="og:site_name" content={site.siteMetadata.site_name}/>
                <link rel="canonical" href={siteUrl}/>
                <meta property="og:title" content="Текст"/>
                <meta property="og:description" content="Текст"/>
            </Helmet>
            <div className="hero-header">
                <div className=""><h1>Привет! Это Юра Никулин <span role="img" aria-label="">🏄‍♂</span></h1></div>
                <div className="primary-content">
                    Да-да, настоящее ФИО <span role="img" aria-label="">🤣</span>. Я маркетолог в it и применяю знания
                    при заработке в интернете. Вышел на
                    доход 1000$ в день и умею прибыльно инвестировать. Смотрите схемы заработка ниже и у вас получится
                    100%<span role="img" aria-label="">👇</span>. Возникнут сложности - пишите в <a
                    href={telegr}>Telegram</a>.
                </div>
                <Link to='/yura-nikulin/' className="button -primary">Написать мне</Link>
            </div>
            <h2>Как я зарабатываю &darr;</h2>
            <p className="text-subscr">Инвестиционные схемы заработка с вложением собественных средств.</p>
            <div className="grids">
                {PostsInvest}
            </div>
            <br/>
            <h2>Схемы заработка &darr;</h2>
            <p>Как заработать на различных схемах в интернете без вложений.</p>
            <div className="grids">
                {PostsIncome}
            </div>
        </Layout>
    )
}

export default IndexPage
export const pageQuery = graphql`
    query indexPageQuery ($img: String, $dir: String)
    {
        site
        {
            siteMetadata
            {
                title
                canonical
                site_name
                image_width
                image_height
                link {
                    telegrAqvaspirt
                }
            }
        }
        allMarkdownRemark(
            sort: { order: DESC, fields:[frontmatter___date]}
        )
        {
            edges
            {
                node
                {
                    id
                    excerpt(pruneLength:250)
                    frontmatter
                    {
                        date(formatString:"MMMM DD, YYYY")
                        path
                        title
                        thumbnail
                        category
                    }
                }
            }
        }
        allFile(filter: {dir: {regex: $dir}, name: {eq: $img}}) {
            edges {
                node {
                    publicURL
                    absolutePath
                }
            }
        }
    }
`