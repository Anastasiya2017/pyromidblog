import React from "react"
import Helmet from "react-helmet"
import {graphql} from 'gatsby'
import Layout from "../components/layout"
import avtorSrc from "../../static/assets/photoAboutMePage.png";

const AboutMePage = ({
                         data: {
                             site
                         },
                     }) => {
    const youTube = site.siteMetadata.link.youTube
    const inst = site.siteMetadata.link.inst
    const telegr = site.siteMetadata.link.telegrPyromid
    let siteUrl = site.siteMetadata.siteUrl
    return (
        <Layout>
            <Helmet>
                <title>Текст</title>
                <meta name="description" content="Текст"/>
                <meta name="keywords" content="текст, текст2"/>
                <meta property="og:url" content={siteUrl + "/yura-nikulin/"}/>
                <meta property="og:type" content="website"/>
                <meta property="og:image" content="Ссылка на изображение"/>
                <meta property="og:image:width" content={site.siteMetadata.image_width}/>
                <meta property="og:image:height" content={site.siteMetadata.image_height}/>
                <meta property="og:site_name" content={site.siteMetadata.site_name}/>
                <link rel="canonical" href={siteUrl + "/yura-nikulin/"}/>
                <meta property="og:title" content="Текст"/>
                <meta property="og:description" content="Текст"/>
            </Helmet>
            <div className="blog-for-me site-wrapper">
                <h1 className="title-text-me">Обо мне</h1>
                <img className="img-page-me" src={avtorSrc} alt=""/>
                <h2 className="title-post">Ниндзя в маркетинге || Вышел на 1000$ в день || Обожаю it </h2>
                <div>
                    <p className="text-me">Привет! Меня зовут Юра Никулин и я зарабатываю деньги на криптовалюте и
                        высокодоходных проектах.
                        Нестандартно подхожу к заработку, применяю свои навыки маркетинга, аналитики и it.
                        В 2020 году вышел на доход 1000$ в день! Я легко могу понять аудиторию проектов, оценить
                        количество
                        инвестиций и проанализировать стоит ли инвестировать сейчас. Все доходы я публично показываю в
                        этом
                        блоке и соц. сетях.
                    </p>
                </div>
            </div>
            <div className="footer">
                <div className="footer-color">
                    <div className="footer-post footer-text">Подписаться <span role="img" aria-label="">👇️</span>
                    </div>
                    <div className=" footer-post">
                        <a href={telegr} className="button -subscriptions">Telegram</a>
                        <a href={inst} className="button -subscriptions">Instagram</a>
                        <a href={youTube} className="button -subscriptions">YouTube</a>
                    </div>
                </div>
            </div>
            <h1 className="title-inst-me"><span role="img" aria-label="">📱️</span>Мой Instagram</h1>
            <div className="elfsight-app-f3b79acc-ee21-4570-949e-ef75f12391ad"></div>

        </Layout>
    )
}
export default AboutMePage
export const pageQuery = graphql`
    query AboutMePageQuery{
        site {
            siteMetadata {
                title
                canonical
                site_name
                image_width
                image_height
                link {
                    youTube
                    inst
                    telegrPyromid
                }
            }
        }
    }
`