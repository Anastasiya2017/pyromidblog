import React from "react"
import Helmet from "react-helmet"
import {graphql, Link} from 'gatsby'
import Layout from "../components/layout"
import PostLink from "../components/post-link";
import Pagination from "../components/Pagination";

export const blogListPlan = graphql`
    query blogListPlan($img: String, $dir: String)
    { site {
        siteMetadata {
            title
            siteUrl
            canonical
            site_name
            image_width
            image_height
            link {
                telegrPyromid
            }
        }
    }
        allMarkdownRemark(
            filter: {frontmatter: {category: {eq: "plan"}}}
            sort: { order: DESC, fields:[frontmatter___date]}
        ){
            edges
            {
                node
                {
                    id
                    excerpt(pruneLength:250)
                    frontmatter{
                        date(formatString:"MMMM DD, YYYY")
                        path
                        title
                        hashtag
                        attention
                        thumbnail
                        metaDescription
                        category
                        pin
                        pinPrior
                    }
                }
            }
        }
        allFile(filter: {dir: {regex: $dir}, name: {eq: $img}}) {
            edges {
                node {
                    publicURL
                    absolutePath
                }
            }
        }
    }
`

class PlanPage extends React.Component {
    render() {
        const skip = this.props.pageContext.skip
        const limit = this.props.pageContext.limit
        const {data} = this.props
        const {siteMetadata} = data.site
        const {currentPage, numPages} = this.props.pageContext
        const blogSlug = '/plan/'
        const isFirst = currentPage === 1
        const isLast = currentPage === numPages
        const prevPage = currentPage - 1 === 1 ? blogSlug : blogSlug + (currentPage - 1).toString() + '/'
        const nextPage = blogSlug + (currentPage + 1).toString() + '/'
        const tagId = (this.props.pageContext.tagId ? this.props.pageContext.tagId : 0);
        let img = data.allFile.edges.filter(item => item.node.absolutePath.indexOf(tagId.thumbnail) !== -1)
        if (img[0] && img[0].node && img[0].node.publicURL) {
            tagId.thumbnail = img[0].node.publicURL
        }
        let siteUrl = siteMetadata.siteUrl
        let props = {
            isFirst,
            prevPage,
            numPages,
            blogSlug,
            currentPage,
            isLast,
            nextPage
        }

        let Posts = data.allMarkdownRemark.edges
            .sort((a, b) => (a.node.frontmatter.pinPrior || 1000) - (b.node.frontmatter.pinPrior || 1000))
            .slice(skip, limit + skip)
            .map(edge => {
                let file = data.allFile.edges.filter(item => item.node.absolutePath.indexOf(edge.node.frontmatter.thumbnail) !== -1)
                if (file[0] && file[0].node && file[0].node.publicURL) {
                    edge.node.frontmatter.thumbnail = file[0].node.publicURL
                }
                return <PostLink key={edge.node.id} post={edge.node} type={"pin"}/>
            })

        const telegr = siteMetadata.telegrPyromid
        return (
            <Layout>
                <Helmet>
                    <title>Текст</title>
                    <meta name="description" content="Текст"/>
                    <meta name="keywords" content="текст, текст2"/>
                    <meta property="og:url" content={siteUrl + "/plan/"}/>
                    <meta property="og:type" content="website"/>
                    <meta property="og:image" content={siteUrl + tagId.thumbnail + '/'}/>
                    <meta property="og:image:width" content={siteMetadata.image_width}/>
                    <meta property="og:image:height" content={siteMetadata.image_height}/>
                    <meta property="og:site_name" content={siteMetadata.site_name}/>
                    <link rel="canonical" href={siteUrl + "/plan/"}/>
                    <meta property="og:title" content="Текст"/>
                    <meta property="og:description" content="Текст"/>
                </Helmet>
                <div className="hero-header"><h1 className="post-title">Мои схемы заработка без вложений <span
                    role="img"
                    aria-label="">🔑</span>
                </h1>
                    <p className="primary-content">Это Юра. Начинал я с различных схем заработка в интернете без
                        вложений, да и сейчас промышляю. Все схемы дублируются во второй канал<a
                            href={telegr}>Telegram</a>.
                        Если
                        хотите накопить
                        стартовый капитал, можете начать с инструкций ниже. А позже я научу вас грамотно приумножать
                        деньги
                        в разделе <Link to='/invest/'>Инвестиции</Link>. Go!
                    </p>
                    <Link to='/yura-nikulin/' className="button -primary">Написать мне</Link>
                </div>
                {!!tagId && (
                    <div>
                        <h2 className="title-post">Топовая схемы &darr;</h2>
                        <div>
                            <a className="post-invest " href={tagId.path + '/'}>
                                <div className="block">
                                    <div className="box-img">
                                        <div className="pin_tag"></div>
                                        <div className="post-img"
                                             style={{backgroundImage: `url(${tagId.thumbnail})`}}>
                                        </div>
                                    </div>
                                </div>
                                <div className="descr-invest">
                                    <div className="post-invest-name ">
                                        {tagId.title}
                                    </div>
                                    <div className="post-invest-text">
                                        {tagId.promoDescrCat}
                                    </div>
                                </div>
                            </a>
                        </div>
                        <br/>
                    </div>
                )}
                <h2 className="title-post">Мои схемы &darr;</h2>
                <div className="grids">
                    {Posts}
                </div>
                <div>
                    <Pagination {...props} />
                </div>
            </Layout>
        )
    }
}
export default PlanPage