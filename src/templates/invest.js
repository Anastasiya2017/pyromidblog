import React from "react"
import Helmet from "react-helmet"
import {graphql, Link} from 'gatsby'
import Layout from "../components/layout"
import PostLink from "../components/post-link";
import Pagination from "../components/Pagination";
import Img from "../../static/assets/post_invest.png"

export const blogListInvest = graphql`
    query blogListInvest($img: String, $dir: String)
    { site {
        siteMetadata {
            title
            canonical
            siteUrl
            site_name
            image_width
            image_height
            link {
                telegrPyromidInvest
                youTube
            }
        }
    }
        allMarkdownRemark(
            filter: {frontmatter: {category: {eq: "invest"}}}
            sort: { order: ASC, fields:[frontmatter___pinPrior]}
        ){
            edges
            {
                node
                {
                    id
                    excerpt(pruneLength:250)
                    frontmatter{
                        date(formatString:"MMMM DD, YYYY")
                        path
                        title
                        hashtag
                        attention
                        thumbnail
                        metaDescription
                        category
                        pin
                        pinPrior
                    }
                }
            }
        }
        allFile(filter: {dir: {regex: $dir}, name: {eq: $img}}) {
            edges {
                node {
                    publicURL
                    absolutePath
                }
            }
        }
    }
`

class InvestPage extends React.Component {
    render() {
        const skip = this.props.pageContext.skip
        const limit = this.props.pageContext.limit
        const {data} = this.props
        const {siteMetadata} = data.site
        const {currentPage, numPages} = this.props.pageContext
        const blogSlug = '/invest/'
        const isFirst = currentPage === 1
        const isLast = currentPage === numPages
        const prevPage = currentPage - 1 === 1 ? blogSlug : blogSlug + '/page-' + (currentPage - 1).toString() + '/'
        const nextPage = blogSlug + '/page-' + (currentPage + 1).toString() + '/'
        const tagId = (this.props.pageContext.tagId ? this.props.pageContext.tagId : 0);
        const telegr = data.site.siteMetadata.link.telegrPyromidInvest
        const youTube = data.site.siteMetadata.link.youTube
        let img = data.allFile.edges.filter(item => item.node.absolutePath.indexOf(tagId.thumbnail) !== -1)
        if (img[0] && img[0].node && img[0].node.publicURL) {
            tagId.thumbnail = img[0].node.publicURL
        }
        let siteUrl = siteMetadata.siteUrl
        let props = {
            isFirst,
            prevPage,
            numPages,
            blogSlug,
            currentPage,
            isLast,
            nextPage
        }

        let Posts = data.allMarkdownRemark.edges
            .sort((a, b) => (new Date(b.node.frontmatter.date)) - new Date((a.node.frontmatter.date)))
            .sort((a, b) => (a.node.frontmatter.pinPrior || 1000) - (b.node.frontmatter.pinPrior || 1000))
            .slice(skip, limit + skip)
            .map(edge => {
                let file = data.allFile.edges.filter(item => item.node.absolutePath.indexOf(edge.node.frontmatter.thumbnail) !== -1)
                if (file[0] && file[0].node && file[0].node.publicURL) {
                    edge.node.frontmatter.thumbnail = file[0].node.publicURL
                }
                return <PostLink key={edge.node.id} post={edge.node} type={"pin"}/>
            })

        return (
            <Layout>
                <Helmet>
                    <title>Текст</title>
                    <meta name="description" content="Текст"/>
                    <meta name="keywords" content="текст, текст2"/>
                    <meta property="og:url" content={siteUrl + "/invest/"}/>
                    <meta property="og:type" content="website"/>
                    <meta property="og:image" content={siteUrl + tagId.thumbnail + '/'}/>
                    <meta property="og:image:width" content={siteMetadata.image_width}/>
                    <meta property="og:image:height" content={siteMetadata.image_height}/>
                    <meta property="og:site_name" content={data.site.siteMetadata.site_name}/>
                    <link rel="canonical" href={siteUrl + "/invest/"}/>
                    <meta property="og:title" content="Текст"/>
                    <meta property="og:description" content="Текст"/>
                </Helmet>
                <div className="hero-header"><h1 className="post-title">Во что я инвестирую
                    <span role="img" aria-label="">🏦</span>
                </h1>
                    <p className="primary-content">Привет, это Юра! Я инвестирую в высокодоходные проекты и зарабатываю
                        уже 1000$ в день. Показываю свои доходы в <a href={telegr}>Telegram</a> и на
                        <a href={youTube}> YouTube</a>. Смотрите проекты в которых мы участвуем ниже
                        <span role="img" aria-label="">👇</span>. Если не понятно, нажимай кнопку.
                    </p>
                    <Link to='/yura-nikulin/' className="button -primary">Написать мне</Link>
                </div>

                <h2 className="title-post">Сейчас инвестируем в &darr;</h2>
                <div><a className="post-invest" href={tagId.path}>
                    <div className="block">
                        <div className="box-img">
                            <div className="pin_tag"></div>
                            <div className="post-img"
                                 style={{backgroundImage: `url(${tagId.thumbnail ? tagId.thumbnail : Img})`}}>
                            </div>
                        </div>
                    </div>
                    <div className="descr-invest">
                        <div className="post-invest-name">
                            {tagId.title ? tagId.title : ""}
                        </div>
                        <div className="post-invest-text">
                            {tagId.promoDescrCat ? tagId.promoDescrCat : 'Инвестиционный проект, который позволяет' +
                                ' зарабатывать 25% в месяц на полном пассиве. Возможна покупка недвижимости или ' +
                                'автомобиля со скидкой до 70%.'}
                        </div>
                    </div>
                </a>
                </div>
                <br/>
                <h2 className="title-post">Как я зарабатываю &darr;</h2>
                <div className="grids">
                    {Posts}
                </div>
                <div>
                    <Pagination {...props} />
                </div>
            </Layout>
        )
    }
}

export default InvestPage