import React from "react"
import Helmet from 'react-helmet';
import {graphql, Link} from "gatsby"
import Layout from "../components/layout"
import avtorSrc from "../../static/assets/photoMeForPost.png";
import instagram from "../images/instagram.svg";
import telegram from "../images/telegram.svg";
import youtube from "../images/youtube.svg";

export default function Template({
                                     data
                                 }) {
    const {siteMetadata} = data.site
    const {frontmatter, html} = data.markdownRemark
    const youTube = data.site.siteMetadata.link.youTube
    const inst = data.site.siteMetadata.link.inst
    const telegr = data.site.siteMetadata.link.telegrPyromidInvest
    let h2 = ''
    let text = ''
    if (html.match(/<h2>.+<\/h2>/gm) !== null) {
        h2 = (html.match(/<h2>.+<\/h2>/gm));
        h2 = h2[0].replace(/(<h2>|<\/h2>)/gm, '');
        text = html.replace(/<h2>.+<\/h2>/gm, '');
    }
    let img = '';
    if (data.allFile.edges[0] && data.allFile.edges[0].node && data.allFile.edges[0].node.publicURL) {
        img = data.allFile.edges[0].node.publicURL;
    }
    let siteUrl = siteMetadata.siteUrl
    const metaDate = frontmatter.type === 'article' ? {
        "@context": "https://schema.org",
        "@type": "Article",
        "headline": frontmatter.title,
        "image": img,
        "datePublished": frontmatter.date
    } : {
        "@context": "https://schema.org",
        "@type": "FAQPage",
        "mainEntity": {
            "@type": "Question",
            "name": h2,
            "acceptedAnswer": {
                "@type": "Answer",
                "text": text
            }
        }
    }
    return (
        <Layout>
            <Helmet>
                <title>{frontmatter.title}</title>
                <meta name="description" content={frontmatter.metaDescription}/>
                <meta name="keywords" content={frontmatter.metaKeywords}/>
                <meta property="og:url" content={siteUrl + frontmatter.path}/>
                <meta property="og:type" content="article"/>
                <meta property="og:image" content={siteUrl + img}/>
                <meta property="og:image:width" content={siteMetadata.image_width}/>
                <meta property="og:image:height" content={siteMetadata.image_height}/>
                <meta property="og:site_name" content={siteMetadata.site_name}/>
                <link rel="canonical" href={siteUrl + frontmatter.path}/>
                <meta property="og:title" content={frontmatter.ogTitle}/>
                <meta property="og:description" content={frontmatter.ogDescription}/>
                <script type="application/ld+json">{JSON.stringify(metaDate)}</script>
            </Helmet>
            <div className="blog-post-container">
                <article className="post">
                    {!frontmatter.thumbnail && (
                        <div className="post-thumbnail">
                            <h1 className="post-title">{frontmatter.title}</h1>
                            {(frontmatter.type === 'article') && (<div className="post-meta">{frontmatter.date}</div>)}
                            {!!frontmatter.hashtag && (<div className="post-hashtag">
                                <Link to={"/" + frontmatter.hashtag + "/"}>#{frontmatter.hashtag}</Link>
                            </div>)}
                            {!!frontmatter.attention && (<div className="post-attention">{frontmatter.attention}</div>)}
                        </div>
                    )}
                    {!!frontmatter.thumbnail && (
                        <div className="post-thumbnail" style={{backgroundImage: `url(${img})`}}>
                            <h1 className="post-title">{frontmatter.title}</h1>
                            {(frontmatter.type === 'article') && (<div className="post-meta">{frontmatter.date}</div>)}
                            {!!frontmatter.hashtag && (<div className="post-hashtag">
                                <Link to={'/' + frontmatter.hashtag + '/'}>#{frontmatter.hashtag}</Link>
                            </div>)}
                            {!!frontmatter.attention && (<div className="post-attention">{frontmatter.attention}</div>)}
                        </div>
                    )}
                    <div className="short-subscribe">
                        <Link to="/yura-nikulin/"><img src={avtorSrc} alt=""/></Link>
                        <div>
                            <Link to="/yura-nikulin/">
                                <div className="site-icon-black site-subscribe-bloc">Юра Никулин</div>
                            </Link>
                            <div className="site-social-black">
                                <a href={inst} target="_blank"><img className="site-icon-black" src={instagram}
                                                                    alt="instagram"/></a>
                                <a href={telegr} target="_blank"><img className="site-icon-black" src={telegram}
                                                                      alt="telegram"/></a>
                                <a href={youTube} target="_blank"><img className="site-icon-black ytb" src={youtube}
                                                                       alt="youtube"/></a>
                            </div>
                        </div>
                    </div>
                    <div className="line-post"></div>
                    <div className="blog-post-content" dangerouslySetInnerHTML={{__html: html}}/>
                    <br/>
                </article>
            </div>
            <div className="footer">
                <div className="footer-color">
                    <div className="footer-post footer-text">Подписаться на новые посты
                        <span role="img" aria-label="">👇️</span>
                    </div>
                    <div className="footer-post">
                        <a href={telegr} className="button -subscriptions">Telegram</a>
                        <a href={inst} className="button -subscriptions">Instagram</a>
                        <a href={youTube} className="button -subscriptions">YouTube</a>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export const pageQuery = graphql`
    query($path: String!, $img: String, $dir: String) {
        site {
            siteMetadata {
                title
                siteUrl
                canonical
                site_name
                image_width
                image_height
                link {
                    youTube
                    inst
                    telegrPyromidInvest
                    telegrAqvaspirt
                    subscr
                }
            }
        }
        markdownRemark(frontmatter: {path: {eq: $path}}) {
            html
            frontmatter {
                date(formatString: "MMMM DD, YYYY")
                path
                title
                metaDescription
                metaKeywords
                ogTitle
                ogDescription
                type
                hashtag
                attention
                thumbnail
            }
        }
        allFile(filter: {dir: {regex: $dir}, name: {eq: $img}}) {
            edges {
                node {
                    publicURL
                }
            }
        }
    }
`