const path = require(`path`)


exports.createPages = async ({actions, graphql, reporter}) => {
    const {createPage} = actions

    // const blogPostTemplate = path.resolve(`src/templates/blogTemplate.js`)
    const blogListQubittech = path.resolve(`src/templates/tag.js`)
    const blogListInvest = path.resolve(`src/templates/invest.js`)
    const blogListPlan = path.resolve(`src/templates/plan.js`)

    const result = await graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
      ) {
        edges {
          node {
            id
            frontmatter {
              date
              type
              path
              category
              template
              title
              metaDescription
              metaKeywords
              ogTitle
              ogDescription
              menu
              button1
              button1URL
              button2
              button2URL
              promoDescrCat
              thumbnail
              text1
              text2
              text3
              attention
              hashtag
              pinPrior
            }
          }
        }
      }
    }
  `)

    // Handle errors
    if (result.errors) {
        reporter.panicOnBuild(`Error while running GraphQL query.`)
        return
    }
    const posts = result.data.allMarkdownRemark.edges
    let name_tag = 'qubittech'
    let blogPostsCount = 0
    let blogPostsTag = 0
    let blogPostsInvest = 0
    let blogPostsPlan = 0
    let tag = 0


    let pagesTag = {};
    posts.forEach((post, index) => {
        const id = post.node.id
        const previous = index === posts.length - 1 ? null : posts[index + 1].node
        const next = index === 0 ? null : posts[index - 1].node

        let img = post.node.frontmatter.thumbnail.split('/')[1].replace('.jpg', '');
        let dir = '/' + post.node.frontmatter.thumbnail.split('/')[0] + '/';
        if (post.node.frontmatter.template !== 'tags') {
            createPage({
                path: post.node.frontmatter.path,
                component: path.resolve(
                    `src/templates/blogTemplate.js`
                ),
                // additional data can be passed via context
                context: {
                    id,
                    previous,
                    next,
                    img,
                    dir,
                },
            })
        }

        // Count blog posts.
        if (post.node.frontmatter.template === 'BlogPost') {
            blogPostsCount++
        }
        if (post.node.frontmatter.category === 'invest') {
            blogPostsInvest++
        }
        if (post.node.frontmatter.category === 'plan') {
            blogPostsPlan++
        }

        if (post.node.frontmatter.hashtag !== null && post.node.frontmatter.hashtag !== '') {
            blogPostsTag++
        }

        if (post.node.frontmatter.hashtag && post.node.frontmatter.hashtag !== '') {
            hashtag = post.node.frontmatter;
            name_tag = hashtag.hashtag.toLowerCase();
            if (name_tag) {
                if (pagesTag.hasOwnProperty(name_tag)) {
                    pagesTag[name_tag] += 1
                } else {
                    pagesTag[name_tag] = 1
                }
            }
        }
        // chek tag
        if (post.node.frontmatter.menu === 'yes') {
            tag = post.node.frontmatter;
            name_tag = tag.title.toLowerCase();
        }
    })

    const postsPerPage = 12
    const numPagesInvest = Math.ceil(blogPostsInvest / postsPerPage)
    const numPagesPlan = Math.ceil(blogPostsPlan / postsPerPage)
    if (pagesTag) {
        for (let t in pagesTag) {
            let pages = Array.apply(null, {length: Math.ceil(pagesTag[t] / postsPerPage)}).map(Number.call, Number)
            for (let i in pages) {
                posts.forEach((post, index) => {
                    if (post.node.frontmatter.template === "tags" &&
                        post.node.frontmatter.title.toLowerCase() === t) {
                        tag = post.node.frontmatter
                    }
                })
                i = +i
                createPage({
                    path: i === 0 ? `/` + t + `/` : `/` + t + `/page-${i + 1}/`,
                    component: blogListQubittech,
                    context: {
                        tagId: tag,
                        slug: t,
                        limit: postsPerPage,
                        skip: i * postsPerPage,
                        numPages: pages.length,
                        currentPage: i + 1,
                    },
                })
            }
        }
    }

    Array.from({length: numPagesInvest}).forEach((post, i) => {
        createPage({
            path: i === 0 ? `/invest/` : `/invest/page-${i + 1}/`,
            component: blogListInvest,
            context: {
                tagId: tag,
                limit: postsPerPage,
                skip: i * postsPerPage,
                numPages: numPagesInvest,
                currentPage: i + 1,
            },
        })
    })

    Array.from({length: numPagesPlan}).forEach((post, i) => {
        createPage({
            path: i === 0 ? `/plan/` : `/plan/page-${i + 1}/`,
            component: blogListPlan,
            context: {
                tagId: tag,
                limit: postsPerPage,
                skip: i * postsPerPage,
                numPages: numPagesPlan,
                currentPage: i + 1,
            },
        })
    })
}
const {createFilePath} = require(`gatsby-source-filesystem`)
exports.onCreateNode = ({node, getNode, actions}) => {
    const {createNodeField} = actions
    if (node.internal.type === `MarkdownRemark`) {
        const path = createFilePath({node, getNode, basePath: `pages`})
        createNodeField({
            node,
            name: `path`,
            value: path,
        })
    }
}