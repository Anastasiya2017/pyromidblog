---
template: BlogPost
path: /invest7
date: 2020-03-07T14:59:36.571Z
title: invest7
metaDescription: Test meta description
thumbnail: Post7/image-7.jpg

ogTitle: Текст
ogDescription: Текст
metaKeywords: новый блог, новый блог pyromid
type: qa
category: invest
hashtag: qubittech
attention: 🛑 Поздно инвестировать 🛑
---
### Code Highlighting
```javascript
import React from "react"
import { Link, useStaticQuery, graphql } from "gatsby"
import Navigation from "../components/navigation"

export default ({ children }) => {
  const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
          }
        }
      }
    `
  )
  return (
    <div className="site-wrapper">
      <header className="site-header">
        <div className="site-title">
          <Link to="/">{data.site.siteMetadata.title}</Link>
        </div>
        <Navigation />
      </header>
      {children}
    </div>
  )
}
```
